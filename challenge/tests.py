from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import challenge

# Create your tests here.
class ChallengeUnitTest(TestCase):
    def test_challenge_url_is_exist(self):
        response = Client().get('/challenge/')
        self.assertEqual(response.status_code, 200)

    def test_challenge_using_challenge_func(self):
        found = resolve('/challenge/')
        self.assertEqual(found.func, challenge)

    def test_challenge_using_challenge_template(self):
        response = Client().get('/challenge/')
        self.assertTemplateUsed(response, 'challenge.html')

    def test_challenge_is_completed(self):
        request = HttpRequest()
        response = challenge(request)
        html_response = response.content.decode('utf8')
        self.assertIn('1806173544', html_response)

