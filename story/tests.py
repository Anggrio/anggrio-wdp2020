from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import homepage, confirmationPage
from .models import Status
from .forms import StatusForm

# Create your tests here.
class StoryUnitTest(TestCase):
    # story 6
    @classmethod
    def setUpTestData(cls):
        Status.objects.create(name = 'test name', status = 'test for new object')

    def test_homepage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_homepage_using_homepage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

    def test_homepage_using_homepage_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage.html')

    def test_homepage_is_completed(self):
        request = HttpRequest()
        response = homepage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, how are you?', html_response)

    def test_homepage_receive_post_request(self):
        response = Client().post('/', {'name': 'dummy', 'status': 'dummy'})
        self.assertEqual(response.status_code, 200)

    def test_homepage_receive_non_post_request(self):
        Client().get('/', {'status': 'dummy'})
        response = Status.objects.all().count()
        self.assertEqual(response, 1)

    def test_status_model_add_object(self):
        count = Status.objects.all().count()
        self.assertEqual(count, 1)

    def test_status_model_str_func(self):
        check = Status.objects.get(pk=1)
        self.assertEqual(f'{check}', 'test for new object')

    def test_status_model_status_field_label(self):
        check = Status.objects.get(pk=1)
        length = check._meta.get_field('status').verbose_name
        self.assertEqual(length, 'status')

    def test_status_model_status_field_length(self):
        check = Status.objects.get(pk=1)
        length = check._meta.get_field('status').max_length
        self.assertEqual(length, 255)

    def test_status_form_is_functioning(self):
        form = StatusForm(data = {'name': 'name', 'status': 'status'})
        self.assertTrue(form.is_valid())

    # story 7
    def test_status_model_name_field_label(self):
        check = Status.objects.get(pk=1)
        length = check._meta.get_field('name').verbose_name
        self.assertEqual(length, 'name')
    
    def test_status_model_name_field_length(self):
        check = Status.objects.get(pk=1)
        length = check._meta.get_field('name').max_length
        self.assertEqual(length, 255)

    def test_confirmation_page_url_is_exist(self):
        response = Client().get('/confirm')
        self.assertEqual(response.status_code, 200)

    def test_confirmation_page_using_confirmationPage_func(self):
        found = resolve('/confirm')
        self.assertEqual(found.func, confirmationPage)

    def test_confirmation_page_using_confirmationPage_template(self):
        response = Client().get('/confirm')
        self.assertTemplateUsed(response, 'confirmationPage.html')

    def test_confirmation_page_is_completed(self):
        request = HttpRequest()
        response = confirmationPage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Please confirm your name and status', html_response)

    def test_confirmation_page_receive_post_request_for_confirm(self):
        response = Client().post('/confirm', {'confirm': 'Submit Status', 'name': 'dummy', 'status': 'dummy'})
        html_response = response.content.decode('utf8')
        self.assertIn('dummy', html_response)
    
    def test_confirmation_page_receive_post_request_for_continue(self):
        Client().post('/confirm', {'continue': 'Submit Status', 'name': 'dummy', 'status': 'dummy'})
        response = Status.objects.all().count()
        self.assertEqual(response, 2)