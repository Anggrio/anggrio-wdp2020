from django.forms import ModelForm, TextInput, Select
from .models import Status

class StatusForm(ModelForm):
    class Meta:
        model = Status
        fields = ['name', 'status']
        widgets = {
            'name': TextInput(attrs={'class': 'form-control'}),
            'status': TextInput(attrs={'class': 'form-control'}),
        }