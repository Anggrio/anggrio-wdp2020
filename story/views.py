from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import StatusForm
from .models import Status

# Create your views here.
def homepage(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = StatusForm(request.POST)
    # if a GET (or any other method) we'll create a blank form
    else:
        form = StatusForm()

    status = Status.objects.all()
    return render(request, 'homepage.html', {'status': status, 'form': form})

def confirmationPage(request):
    if request.method == 'POST':
        if 'confirm' in request.POST:
            form = StatusForm(request.POST)
            if form.is_valid():
                name = form.cleaned_data['name']
                status = form.cleaned_data['status']
                return render(request, 'confirmationPage.html', {'name': name, 'status': status})
        elif 'continue' in request.POST:
            form = StatusForm(request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect('/')

    return render(request, 'confirmationPage.html', {'name': "", "status": ""})
    